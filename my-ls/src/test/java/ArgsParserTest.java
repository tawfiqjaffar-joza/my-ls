import exception.IllegalOptionException;
import listener.ArgsParserListener;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.junit.Assert;
import org.junit.Test;
import presenter.ArgsParserPresenter;

public class ArgsParserTest implements ArgsParserListener {

    private CommandLine cmd;
    private Options options;

    @Override
    public void saveArgs(CommandLine cmd, Options options) {
        this.cmd = cmd;
        this.options = options;
    }

    @Test
    public void test01_GIVEN_command_line_WHEN_parsed_THEN_command_options_returned() {
        // GIVEN
        String[] commandLineStr = {"-a", "~/Desktop"};
        ArgsParserPresenter presenter = new ArgsParserPresenter(this);

        // WHEN
        try {
            presenter.parseArgs(commandLineStr);
        } catch (IllegalOptionException | ParseException ignored) {
        }
        // THEN

        Assert.assertTrue(this.options.hasOption("-a"));
        Assert.assertTrue(this.cmd.getArgList().contains("~/Desktop"));
    }

    @Test
    public void test02_GIVEN_command_line_invalid_option_WHEN_parsed_THEN_throws_exception() {
        // GIVEN
        String[] commandLineStr = {"-o"};
        ArgsParserPresenter presenter = new ArgsParserPresenter(this);
        boolean didThrow = false;

        // WHEN
        try {
            presenter.parseArgs(commandLineStr);
        } catch (IllegalOptionException | ParseException ignored) {
            didThrow = true;
        }
        // THEN

        Assert.assertTrue(didThrow);
    }

    @Test
    public void test03_GIVEN_command_line_multiple_args_WHEN_parsed_THEN_command_options_returned() {
        // GIVEN
        String[] commandLineStr = {"folder1", "folder2", "folder3"};
        ArgsParserPresenter presenter = new ArgsParserPresenter(this);

        // WHEN
        try {
            presenter.parseArgs(commandLineStr);
        } catch (IllegalOptionException | ParseException ignored) {
        }
        // THEN

        Assert.assertEquals(3, this.cmd.getArgList().size());
    }

}
