package view;

import exception.IllegalOptionException;
import exception.PathNotFoundException;

public final class App {
    public static void runApp(String[] args) {
        ArgsParser argsParser = new ArgsParser(args);
        try {
            argsParser.parseArgs();
            LS ls = new LS(argsParser.getCommandLine(), argsParser.getOptions());
            ls.run();
        } catch (IllegalOptionException | PathNotFoundException e) {
            System.out.println(e.getMessage());
        }
    }
}