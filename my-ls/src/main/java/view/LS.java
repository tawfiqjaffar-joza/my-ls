package view;

import exception.PathNotFoundException;
import listener.LSListener;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;
import presenter.LSPresenter;

import java.io.PrintWriter;


public class LS implements LSListener {
    private CommandLine cmd;
    private Options options;
    private LSPresenter presenter;

    public LS(CommandLine optionList, Options options) {
        this.cmd = optionList;
        this.options = options;
        this.presenter = new LSPresenter(this);
    }

    public void run() throws PathNotFoundException {
        this.presenter.runOptions(this.cmd);
    }

    @Override
    public void printHelp() {
        HelpFormatter helpFormatter = new HelpFormatter();
        PrintWriter writer = new PrintWriter(System.out);
        helpFormatter.printUsage(writer, 80, "ls", this.options);
        writer.flush();
    }

    @Override
    public void displayError(Throwable t) {
        System.out.println(t.getMessage());
    }

    @Override
    public void displayResult(String result) {
        System.out.print(result);
    }
}
