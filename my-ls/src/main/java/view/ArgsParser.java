package view;

import exception.IllegalOptionException;
import listener.ArgsParserListener;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import presenter.ArgsParserPresenter;

public class ArgsParser implements ArgsParserListener {
    private String[] args;
    private ArgsParserPresenter presenter;
    private CommandLine cmd;
    private Options options;

    public ArgsParser(String[] args) {
        this.args = args;
        this.presenter = new ArgsParserPresenter(this);
    }

    public void parseArgs() throws IllegalOptionException {
        try {
            this.presenter.parseArgs(this.args);
        } catch (ParseException e) {
            throw new IllegalOptionException(e.getMessage());
        }
    }

    public CommandLine getCommandLine() {
        return cmd;
    }


    public Options getOptions() {
        return this.options;
    }


    @Override
    public void saveArgs(CommandLine optionList, Options options) {
        this.cmd = optionList;
        this.options = options;
    }


}
