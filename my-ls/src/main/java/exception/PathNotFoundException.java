package exception;

public class PathNotFoundException extends Exception {
    private String message;

    public PathNotFoundException(String message) {
        super(message);
        this.message = message;
    }

}
