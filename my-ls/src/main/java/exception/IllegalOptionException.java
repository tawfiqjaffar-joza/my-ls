package exception;

public class IllegalOptionException extends Exception {
    private String message;

    public IllegalOptionException(String message) {
        super(message);
        this.message = message;
    }

}
