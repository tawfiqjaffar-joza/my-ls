package listener;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.Options;

public interface ArgsParserListener {
    public void saveArgs(CommandLine cmd, Options options);
}
