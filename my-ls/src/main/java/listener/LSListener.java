package listener;

public interface LSListener {
    void printHelp();

    void displayError(Throwable t);

    void displayResult(String result);
}
