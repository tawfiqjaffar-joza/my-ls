import view.App;

public class Main {

    public static void main(String[] args) {
        App.runApp(args);
    }

}
