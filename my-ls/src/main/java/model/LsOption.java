package model;

public class LsOption {

    public static String optionToString(Option option) {
        switch (option) {
            case HELP:
                return "h";
            case LONG:
                return "l";
            case ALL:
                return "a";
            default:
                return "";
        }
    }
}
