package presenter;

import exception.PathNotFoundException;
import listener.LSListener;
import model.LsOption;
import model.Option;
import org.apache.commons.cli.CommandLine;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;

public class LSPresenter {
    private final LSListener listener;

    public LSPresenter(LSListener listener) {
        this.listener = listener;
    }

    public void runOptions(CommandLine cmd) {
        if (cmd.hasOption(LsOption.optionToString(Option.HELP))) {
            this.listener.printHelp();
        } else {
            this.readDirectory(cmd);
        }
    }

    private void readDirectory(CommandLine cmd) {
        final List<String> paths = cmd.getArgList();

        if (paths.isEmpty()) {
            paths.add(".");
        }
        paths.forEach(s -> {
            try {
                s = s.replaceFirst("^~", System.getProperty("user.home"));
                checkIfPathExists(s);
                this.readFilesInDirectory(s, paths.size() > 1);
            } catch (PathNotFoundException pathNotFoundException) {
                this.listener.displayError(pathNotFoundException);
            }
        });
    }

    private void checkIfPathExists(final String stringPath) throws PathNotFoundException {
        final Path path = Path.of(stringPath);

        if (!Files.exists(path)) {
            throw new PathNotFoundException(String.format("ls: %s: No such file or directory", stringPath));
        }
    }

    private void readFilesInDirectory(String stringPath, Boolean multi) {
        if (multi) {
            this.listener.displayResult(stringPath + ":\n");
        }
        File file = new File(stringPath);
        boolean isFirst = true;
        File[] files = file.listFiles();
        Arrays.sort(Objects.requireNonNull(files), Comparator.comparingInt(o -> o.getName().charAt(0)));
        for (final File fileEntry : Objects.requireNonNull(file.listFiles())) {
            if (!isFirst) {
                this.listener.displayResult(" ");
            }
            this.listener.displayResult(fileEntry.getName());
            isFirst = false;
        }
        this.listener.displayResult(multi ? "\n\n" : "\n");
    }
}
