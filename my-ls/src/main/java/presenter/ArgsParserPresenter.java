package presenter;

import exception.IllegalOptionException;
import listener.ArgsParserListener;
import org.apache.commons.cli.*;

public class ArgsParserPresenter {

    private ArgsParserListener listener;

    public ArgsParserPresenter(ArgsParserListener listener) {
        this.listener = listener;
    }

    public void parseArgs(String[] argList) throws IllegalOptionException, ParseException {
        Options options = new Options();

        options.addOption("a", "all", false, "do not ignore entries starting with .")
                .addOption("l", false, "use a long listing format")
                .addOption("h", "help", false, "help dialog");

        CommandLineParser parser = new DefaultParser();
        CommandLine cmd = parser.parse(options, argList);

        this.listener.saveArgs(cmd, options);
    }
}
